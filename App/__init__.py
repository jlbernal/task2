from flask import Flask, Blueprint
from flask_restplus import Api

app = Flask(__name__)
api = Api(app)


# Need to be here to be able to import service properly
from App import service
