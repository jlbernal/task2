from App import api, app
from flask_restplus import Resource, fields
from models.restaurants import Restaurant, RestaurantPair, RestaurantCombos, RestaurantDao
from models.utils import FloatConverter, validate_geo_coordinates

app.url_map.converters['float'] = FloatConverter

model_restaurant = api.model('Restaurant', {
    'id': fields.Integer,
    'name': fields.String,
    'city': fields.String,
    'cuisine': fields.List(fields.String),
    'country': fields.String,
    'latitude': fields.Float,
    'longitude': fields.Float,
})

model_restaurant_pair = api.model('RestaurantPair', {
    'first': fields.Nested(model_restaurant),
    'second': fields.Nested(model_restaurant)
})

model_restaurant_combos = api.model('RestaurantCombos', {
    'both': fields.List(fields.Nested(model_restaurant)),
    'pairs': fields.List(fields.Nested(model_restaurant_pair))
})

# Data access object
restaurant_dao = RestaurantDao()


# Api routes
@api.route('/restaurants/near/<float:latitude>/<float:longitude>')
class RestaurantsNearCoordinate(Resource):
    @api.param('latitude', 'The coordinate latitude')
    @api.param('longitude', 'The coordinate longitude')
    @api.marshal_list_with(model_restaurant)
    def get(self, latitude, longitude):
        return (restaurant_dao.search_near_coordinates(latitude, longitude)
                if validate_geo_coordinates(latitude, longitude) else [])


@api.route('/restaurants/near/<string:address>')
class RestaurantsNearAddress(Resource):
    @api.param('address', 'The address to look up')
    @api.marshal_list_with(model_restaurant)
    def get(self, address):
        return restaurant_dao.search_near_address(address)


@api.route('/restaurants/combo/<string:cuisine_a>/<string:cuisine_b>')
class RestaurantsSearchCombo(Resource):
    @api.param('cuisine_a', 'Cuisine type')
    @api.param('cuisine_b', 'Cuisine type')
    @api.marshal_list_with(model_restaurant_combos)
    def get(self, cuisine_a, cuisine_b):
        return restaurant_dao.search_combo(cuisine_a, cuisine_b)
