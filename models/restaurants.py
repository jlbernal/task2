import os, sys
from json import load
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from itertools import product


class Restaurant:
    """ Model of restaurants
    """
    def __init__(self, id, name, cuisine, lat, lon, city, country):
        self._id = id
        self._name = name
        self._cuisine = cuisine
        self._lat = lat
        self._lon = lon
        self._city = city
        self._country = country

    @property
    def id(self):
        return self._id

    @property
    def name(self):
        return self._name

    @property
    def cuisine(self):
        return self._cuisine

    @property
    def latitude(self):
        return self._lat

    @property
    def longitude(self):
        return self._lon

    @property
    def city(self):
        return self._city

    @property
    def country(self):
        return self._country


class RestaurantPair:
    """ Model of restaurants pairs
    """
    def __init__(self, first, second):
        self._first = first
        self._second = second

    @property
    def first(self):
        return self._first

    @property
    def second(self):
        return self._second


class RestaurantCombos:
    """ Model of return restaurants combination request
    """
    def __init__(self, both, pairs):
        self._both = both
        self._pairs = pairs

    @property
    def both(self):
        return self._both

    @property
    def pairs(self):
        return self._pairs


class RestaurantDao(object):
    """ Restaurant Data access object
        """

    def from_json(self, element):
        """ Convert json object into a restaurant element
        """
        cuisinesElem = element['tags'].get('cuisine')
        cuisines = cuisinesElem.split(';') if (cuisinesElem) else []
        return Restaurant(element['id'], element['tags'].get('name'), cuisines, element['lat'], element['lon'],
                          element['tags'].get('addr:city'), element['tags'].get('addr:country'))

    def __init__(self):
        file_name = "data/berlin_restaurants.json"
        path_app = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        path_file = os.path.join(path_app, file_name)
        with open(path_file, "r") as file:
            self.data = list(map(lambda element: self.from_json(element), load(file)))
            self.geocoder = Nominatim(user_agent="my-application")
            self.max_distance = 10
            self.max_number_restaurants = 10

    def search_near_coordinates(self, latitude, longitude):
        """ Search for near locations around geographical coordinates .
            Args:
                latitude: geographical coordinates latitude
                longitude: geographical coordinates longitude
            Returns:
                list of near locations to the reference point
        """
        all_distances = map(lambda element: (
            element, geodesic((latitude, longitude), (element.latitude, element.longitude)).kilometers), self.data)

        filtered = filter(lambda tuple: tuple[1] < self.max_distance, all_distances)

        result = sorted(filtered, key=lambda tuple: tuple[1])[:self.max_number_restaurants]
        return list(map(lambda my_tuple: my_tuple[0], result))

    def search_near_address(self, address):
        """ Search for near locations around an address.
            Args:
                address: string with the address
            Returns:
                list of near locations to the address
        """
        location = self.geocoder.geocode(address)
        return self.search_near_coordinates(location.latitude, location.longitude) if location else []

    def search_combo(self, cuisine_a, cuisine_b):
        """ Search for near locations around an address.
            Args:
                cuisine_a: string with first type of cuisine
                cuisine_b: string with second type of cuisine

            Returns:
                tuple with list of locations with both cousines and list of pairs near restaurants
        """

        # Get all restaurant with only type a of cuisines
        of_type_a = list(filter(lambda x: cuisine_a in x.cuisine and not cuisine_b in x.cuisine, self.data))

        # Get all restaurant with only type b of cuisines
        of_type_b = list(filter(lambda x: cuisine_b in x.cuisine and not cuisine_a in x.cuisine, self.data))

        # Get all restaurant with both cuisines
        both = list(filter(lambda x: cuisine_a in x.cuisine and cuisine_b in x.cuisine, self.data))

        # Compute all distance among restaurant with differents cuisines
        cartesion_product = map(lambda my_tuple: (my_tuple[0], my_tuple[1], geodesic((my_tuple[0].latitude, my_tuple[0].longitude), (
            my_tuple[1].latitude, my_tuple[1].longitude)).kilometers), product(of_type_a, of_type_b))

        sorted_by_distance = sorted(cartesion_product, key=lambda my_tuple: my_tuple[2])

        return RestaurantCombos(both[:self.max_number_restaurants],
                                list(map(lambda x: RestaurantPair(x[0], x[1]), sorted_by_distance))[
                                :self.max_number_restaurants])
