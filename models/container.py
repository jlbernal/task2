# File deprecated
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from models.utils import read_json


class LocalsContainer(object):

    def __init__(self, data_location):
        """ Constructor.

            Args:
                data_location: relativa path to the data to be loaded
            Returns:
                None
        """
        self.data = read_json(data_location)


    def search_near_location_coordinates(self, reference_point, max_distance=10, max_number_locations=10):
        """ Search for near locations around geographical coordinates .

            Args:
                reference_point: geographical coordinates (latitude, longitude) to begin search
                max_distance: maximum distance in km.
                max_number_locations: maximum number of location to returned
            Returns:
                list of near locations to the reference point
        """
        distances_dict = []

        #calculate the distance form reference point  to each restaurant
        for index, element in enumerate(self.data, 0):
            element_coordinates_point = (element["lat"], element["lon"])
            distance = geodesic(reference_point, element_coordinates_point).kilometers

            #Filter only restaurant that are 'near' up to maximum distance
            if distance <= max_distance:
                distances_dict.append({"restaurantIdx": index, "distance": distance})

        #sort locations by distance
        distances_dict = sorted(distances_dict, key=lambda k: k["distance"])
        near_locals = []

        if distances_dict:
            for index in range(max_number_locations):
                near_locals.append(self.data[distances_dict[index]["restaurantIdx"]])

        return near_locals

    def search_near_locations_tags(self, tag_name, search_tag_value1, search_tag_value2, max_number_locals=10):
        near_locals = []
        locals_tag_value1 = []
        locals_tag_value2 = []

        for element in self.data:
            if tag_name in element['tags']:
                current_tag_value = element['tags'][tag_name]

                if (search_tag_value1 in current_tag_value) and (search_tag_value2 in current_tag_value):
                    near_locals.append((element, None))

                elif search_tag_value1 == current_tag_value:
                    locals_tag_value1.append(element)

                elif search_tag_value2 == current_tag_value:
                    locals_tag_value2.append(element)

        # number of restaurants with both tags
        current_locals_founded = len(near_locals)

        # verify if reach max_locals and
        if current_locals_founded >= max_number_locals:
            return near_locals[:max_number_locals]

        elif locals_tag_value1 and locals_tag_value2:
            distances = []
            for idx1 in range(len(locals_tag_value1)):
                for idx2 in range(len(locals_tag_value2)):
                    point1 = (locals_tag_value1[idx1]["lat"], locals_tag_value1[idx1]["lon"])
                    point2 = (locals_tag_value2[idx2]["lat"], locals_tag_value2[idx2]["lon"])
                    distance_value = geodesic(point1, point2).kilometers
                    distances.append({"c1_idx": idx1, "c2_idx": idx2, "distance": distance_value})

            distances = sorted(distances, key=lambda k: k['distance'])
            available = max_number_locals - current_locals_founded

            for index in range(available):
                near_locals.append([locals_tag_value1[distances[index]['c1_idx']],
                                    locals_tag_value2[distances[index]['c2_idx']]])

        return near_locals


    def search_near_locations_address(self, request_address):
        geo_locator = Nominatim(user_agent="test")
        location = geo_locator.geocode(request_address)
        if location:
            print(location.latitude, location.longitude)
            point = (location.latitude, location.longitude)
            return self.search_near_location_coordinates(point)
        else:
            return {}