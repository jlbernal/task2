import os
import sys
import json
from werkzeug.routing import FloatConverter as BaseFloatConverter


class FloatConverter(BaseFloatConverter):
    """" A class used to allow negative values for floats on routes
    """
    regex = r'-?\d+(\.\d+)?'


def read_json(file_name):
    """ Read a json list from a file.

        Args:
            file_name: name of the file to be read. relative to project folder

        Returns:
            A dictionary with de data contained on the file.
    """
    path_app = os.path.dirname(sys.argv[0])
    path_file = os.path.join(path_app,file_name)
    with open(path_file, "r") as file:
        data = json.load(file)
    return data


def validate_geo_coordinates(latitude, longitude):
    """ Validate if the geographic coordinates are valid.

        Args:
            latitude:  latitude coordinate
            longitude: longitude coordinate

        Returns:
            A boolean informing is the coordinate pair is valid or not
    """
    if latitude > 90 or latitude < -90:
        return False
    if longitude > 180 or longitude < -180:
        return False
    return True


def set_return_value(result_list, error_dict):
    """ Set the return value for the response.

        Args:
            result_list: list of dictionary of objects
            error_dict:  dictionary containing the message to response in case of non object founded

        Returns:
            The list or error message
    """
    return result_list if result_list else error_dict

