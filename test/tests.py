import unittest
from unittest.mock import MagicMock


from models.utils import validate_geo_coordinates
from geopy.distance import geodesic
from geopy.geocoders import Nominatim
from geopy.point import Point

from models.restaurants import Restaurant, RestaurantDao


class TestValidateGeoCoordinates(unittest.TestCase):

    def test_invalid_coordinates(self):
        self.assertFalse(validate_geo_coordinates(-100, 50))

    def test_valid_coordinates(self):
        self.assertTrue(validate_geo_coordinates(89, -120))


class TestRestaurant(unittest.TestCase):
    def setUp(self):
        pass

    def test_restaurant_init(self):
        id = 26735759
        name = 'Madame Ngo'
        cuisines = ['sushi', 'cuban']
        latitude = 52.5062068
        longitude = 13.3180919
        city = 'Berlin'
        country = 'DE'
        restaurant = Restaurant(id, name, cuisines, latitude, longitude, city, country)

        self.assertEqual(id, restaurant.id)
        self.assertEqual(name, restaurant.name)
        self.assertEqual(cuisines, restaurant.cuisine)
        self.assertEqual(latitude, restaurant.latitude)
        self.assertEqual(longitude, restaurant.longitude)
        self.assertEqual(city, restaurant.city)
        self.assertEqual(country, restaurant.country)


class TestRestaurantDao(unittest.TestCase):
    def setUp(self):
        pass

    def test_init_loads_data(self):
        dao = RestaurantDao()
        self.assertEqual(3931, len(dao.data))
        pass

    def test_search_near_coordinates_returns_correct_values(self):
        dao = RestaurantDao()
        result = dao.search_near_coordinates(52.502963, 13.2882287)
        self.assertEqual(598185240, result[0].id)
        self.assertEqual(29997724, result[4].id)
        self.assertEqual(715911674, result[9].id)

    def test_search_near_coordinates_returns_10_or_less_elements(self):
        dao = RestaurantDao()
        result = dao.search_near_coordinates(52.502963, 13.2882287)
        self.assertEqual(10, len(result))

    def test_search_near_coordinates_returns_empty_if_distance_is_greater_than_10km(self):
        dao = RestaurantDao()
        result = dao.search_near_coordinates(48.8566, 2.3522)
        self.assertEqual(0, len(result))

    def test_search_near_coordinates_returns_sorted_by_distance(self):
        point = (52.502963, 13.2882287)
        dao = RestaurantDao()
        result = dao.search_near_coordinates(point[0], point[1])

        prev = 0.0
        for element in result:
            curr = geodesic((point[0], point[1]), (element.latitude, element.longitude)).kilometers
            self.assertLessEqual(prev, curr)
            prev = curr

    def test_search_near_address_geodecode_address_and_then_do_search_near_coordinates(self):
        address = 'Dernburgstr, Berlin, Germany'
        location = Point(52.502963, 13.2882287, 0)

        geocoder = Nominatim(user_agent="my-application")
        geocoder.geocode = MagicMock(return_value=location)

        dao = RestaurantDao()
        dao.geocoder = geocoder
        dao.search_near_coordinates = MagicMock(return_value=[])

        dao.search_near_address(address)
        geocoder.geocode.assert_called_with(address)
        dao.search_near_coordinates.assert_called_with(location.latitude, location.longitude)

if __name__ =='__main__' :
    unittest.main()
